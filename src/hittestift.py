#!/usr/bin/python3

import os
import warnings
from typing import Tuple, Optional, Union, List, Callable
from nptyping import Array

import gi  # type: ignore
gi.require_version('Gtk', '3.0')
from gi.repository import Gio, Gtk, GLib, GdkPixbuf, Gdk
from PIL import Image, ImageDraw
import json
import numpy as np
from matplotlib import cm
from shapely.geometry import Polygon

import flir_image_extractor as flir
from annotation import Annotation


class Hittestift(Gtk.Application):
    BASE_RES: Tuple[int, int] = (640, 480)

    multiplier: float = 1.0
    thermograms_folder = None
    thl: Optional[GdkPixbuf.Pixbuf]     = None
    thl_src: Optional[Array[float]]     = None
    opt: Optional[GdkPixbuf.Pixbuf]     = None
    opt_src: Optional[GdkPixbuf.Pixbuf] = None

    leaks: List[Annotation] = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id="eu.nimmerfort.Hittestift",
                         flags=Gio.ApplicationFlags.FLAGS_NONE, **kwargs)

    def do_activate(self):
        res_dir = get_resources_directory()
        builder = Gtk.Builder.new_from_file(os.path.join(res_dir,
                                                         "hittestift.ui"))
        builder.connect_signals(self)

        screen = Gdk.Screen.get_default()
        css = Gtk.CssProvider()
        css.load_from_path(os.path.join(res_dir, "theme.css"))
        Gtk.StyleContext.add_provider_for_screen(screen, css, Gtk.STYLE_PROVIDER_PRIORITY_USER)

        self.main_window = builder.get_object("main_window")
        self.img_panels = builder.get_object("img_panels")
        self.img_left = builder.get_object("img_left")
        self.img_right = builder.get_object("img_right")
        self.thl_min = builder.get_object("val_min")
        self.thl_max = builder.get_object("val_max")
        self.scale_min = builder.get_object("scale_min")
        self.scale_max = builder.get_object("scale_max")
        self.filter = builder.get_object("Thermogrammen")
        self.btn_save = builder.get_object("btn_save")
        self.btn_warmteverlies = builder.get_object("btn_warmteverlies")
        self.btn_beoordelingsgebied = builder.get_object("btn_beoordelingsgebied")

        self.add_window(self.main_window)
        self.main_window.present()
        self.panels_ratio = 0.5
        self.set_panels_ratio(ratio=0.5)

    def open_thermogram(self, path):
        self.thl_src, self.opt_src = self.decompose_thermogram(path)
        self.leaks = []
        self.render_thermogram()
        self.enable_ui()

    def render_thermogram(self):
        if not self.thermogram_open():
            return

        res = tuple(int(dim * self.multiplier) for dim in self.BASE_RES)
        if self.thl_src is not None:
            min_val = self.thl_min.get_value()
            max_val = self.thl_max.get_value()
            thl = colorize_thermogram(self.thl_src, min_val, max_val)
            draw_polygons(thl, [l.polygon for l in self.leaks])
            draw_annotation(thl, self.partial_polygon)
            self.thl = image2pixbuf(thl, res)
            self.img_left.set_from_pixbuf(self.thl)

        if self.opt_src is not None:
            self.opt = scale_pixbuf(self.opt_src, res)
            self.img_right.set_from_pixbuf(self.opt)

    def enable_ui(self):
        self.scale_min.set_sensitive(True)
        self.scale_max.set_sensitive(True)
        self.btn_warmteverlies.set_sensitive(True)
        self.btn_beoordelingsgebied.set_sensitive(True)
        self.btn_save.set_sensitive(True)

    def on_quit(self, *_):
        """ Callback for the Quit menu item that quits this application.
        """
        self.quit()

    def choose_thermogram(self, *_):
        action = Gtk.FileChooserAction.OPEN
        chooser = Gtk.FileChooserNative()
        chooser.add_filter(self.filter) # forces uses of non-native dialog. TODO only do on Linux
        if self.thermograms_folder and os.path.isdir(self.thermograms_folder):
            chooser.set_current_folder(self.thermograms_folder)

        result = chooser.run()
        if result == Gtk.ResponseType.ACCEPT:
            thermogram_path = chooser.get_filename()
            self.thermograms_folder = os.path.dirname(thermogram_path)
            self.open_thermogram(thermogram_path)

    def on_scroll(self, widget, event):
        _, x, y = event.get_scroll_deltas()

        if y < 0 and self.multiplier < 2.5: # enlarge
            self.multiplier += 0.05
        elif y > 0 and self.multiplier > 0.10:
            self.multiplier -= 0.05
        else:
            return True
        self.render_thermogram()
        return True

    def change_bounds(self, _):
        self.render_thermogram()

    def motions(self, _):
        #https://stackoverflow.com/questions/43948186/scrolledwindow-scroll-by-drag-simulate-finger-on-touchscreen
        pass

    def decompose_thermogram(self, path: str) -> Tuple[Array, GdkPixbuf.Pixbuf]:
        try:
            decomposer = verify_environment()
            decomposer.process_image(path)
            thermal = decomposer.get_thermal_np()
            optical = decomposer.extract_embedded_image()
            return thermal, image2pixbuf(Image.fromarray(optical))
        except FileNotFoundError as e:
            print("Couldn't find file", str(e))
        return None, None

    def on_window_resize(self, *_):
        width  = self.img_panels.get_allocated_width()
        self.img_panels.set_position(self.panels_ratio * width)

    def get_panels_ratio(self):
        handle = self.img_panels.get_position()
        width  = self.img_panels.get_allocated_width()
        return handle/width

    def set_panels_ratio(
            self,
            _1: Optional[Gtk.Paned] = None,
            _2: Optional[Gdk.Event] = None,
            ratio: Optional[float] = None
    ):
        if ratio is None:
            ratio = self.get_panels_ratio()
        self.panels_ratio = ratio

    def on_motion(self, _, event: Gdk.EventMotion):
        x = event.x / self.multiplier
        y = event.y / self.multiplier

        for point in self.partial_polygon:
            if match_coords(point, (x, y)):
                pass # hover animation

    def thermogram_open(self) -> bool:
        return self.thl_src is not None and self.opt_src is not None

    def on_bewaren(self, *_):
        action = Gtk.FileChooserAction.SAVE
        print(action)
        chooser = Gtk.FileChooserNative()
        chooser.set_action(action)

        result = chooser.run()
        if result == Gtk.ResponseType.ACCEPT:
            save_path = chooser.get_filename()
            with open(save_path, "w") as sink:
                sink.write(json.dumps([ann.dict() for ann in self.leaks]))

    # Annotation code
    annotation_creation_delegate: Optional[Callable[[Tuple[int,int]], Annotation]] = None
    partial_polygon: List[Tuple[int, int]] = []
    annotating = False
    def on_img_clicked(self, eventbox: Gtk.EventBox, event: Gdk.EventButton):
        if not self.thermogram_open:
            return

        if self.annotating:
            self.add_polygon_point(eventbox, event)

    def add_polygon_point(self, eventbox: Gtk.EventBox, event: Gdk.EventButton):
        partial = self.partial_polygon
        x = event.x / self.multiplier
        y = event.y / self.multiplier
        p = (x, y)

        if len(partial) > 2 and match_coords(partial[0], p):
            # If clicked first polygon, close it
            coords = partial.copy()
            coords.append(partial[0])
            leak = self.annotation_creation_delegate(coords)
            self.leaks.append(leak)
            self.reset_annotating()
        else:
            partial.append(p)
        self.render_thermogram()

    def annotate_beoordelingsgebied(self, button: Gtk.Button):
        if self.thermogram_open():
            self.reset_annotating(False)
            self.annotating = button.get_active()
            delegate = lambda points: Annotation("Beoordelingsgebied", points)
            self.annotation_creation_delegate = delegate
        else:
            self.reset_annotating()

    def annotate_warmteverlies(self, button: Gtk.Button):
        if self.thermogram_open():
            self.reset_annotating(False)
            self.annotating = button.get_active()
            delegate = lambda points: Annotation("Warmteverlies", points)
            self.annotation_creation_delegate = delegate
        else:
            self.reset_annotating()

    def reset_annotating(self, reset_buttons: bool = True):
        self.annotating = False
        if reset_buttons:
            self.btn_beoordelingsgebied.set_active(False)
            self.btn_warmteverlies.set_active(False)
        self.partial_polygon = []
        self.render_thermogram()


def match_coords(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    return x1 - 5 < x2 < x1 + 5 and y1 - 5 < y2 < y1 + 5


def draw_polygons(img: Image, polygons: List[Polygon]):
    drw = ImageDraw.Draw(img, 'RGB')
    [drw.line(p.exterior.coords, fill=(255, 0, 0), width=2) for p in polygons]
    [drw.ellipse(
        [p[0]-5,p[1]-5,p[0]+5,p[1]+5],
        fill=(255, 0, 0),
        outline=(0, 0, 0)
    ) for polygon in polygons for p in polygon.exterior.coords]
    del drw


def draw_annotation(img: Image, points: List[Tuple[int,int]]):
    drw = ImageDraw.Draw(img, 'RGB')
    drw.line(points, fill=(255, 255, 255), width=2)
    [drw.ellipse(
        [p[0]-5,p[1]-5,p[0]+5,p[1]+5],
        fill=(255, 255, 255),
        outline=(0, 0, 0)
    ) for p in points]
    del drw


def get_resources_directory():
    return os.path.join(os.path.join(get_program_directory(), ".."),
                        "resources")


def get_program_directory():
    return os.path.dirname(os.path.abspath(__file__))


def pixbuf2image(pix):
    """Convert gdkpixbuf to PIL image"""
    data = pix.get_pixels()
    w = pix.props.width
    h = pix.props.height
    stride = pix.props.rowstride
    mode = "RGB"
    if pix.props.has_alpha == True:
        mode = "RGBA"
    im = Image.frombytes(mode, (w, h), data, "raw", mode, stride)
    return im


def image2pixbuf(img, size=None):
    """Convert Pillow image to GdkPixbuf"""
    data = img.tobytes()
    w, h = img.size
    data = GLib.Bytes.new(data)
    pix = GdkPixbuf.Pixbuf.new_from_bytes(data, GdkPixbuf.Colorspace.RGB,
            False, 8, w, h, w * 3)
    if size and len(size) == 2 and size != (w, h):
        pix = pix.scale_simple(size[0], size[1], GdkPixbuf.InterpType.BILINEAR)
    return pix


def scale_pixbuf(pixbuf, scale: Union[float, Tuple[int, int]]):
    if isinstance(scale, float):
        w, h = pixbuf.get_width(), pixbuf.get_height()
        new_w, new_h = scale * w, scale * h
    elif isinstance(scale, tuple) and len(scale) == 2:
        new_w, new_h = scale
    else:
        raise ValueError("Unexpected and invalid value/type for scale")
    return pixbuf.scale_simple(new_w, new_h, GdkPixbuf.InterpType.BILINEAR)


def colorize_thermogram(thermal, minp, maxp):
    # Find the boundary values for the thermogram values with percentiles
    if 0 <= minp <= 1.0 == False:
        minp = 0.1 # sane defaults against illegal values
    if 0 <= maxp <= 1.0 == False:
        maxp = 0.9 # sane defaults against illegal values

    min_perc = np.percentile(thermal, int(minp * 100))
    max_perc = np.percentile(thermal, int(maxp * 100))

    # Bound the thermogram with those min and max values and normalize
    thermal_normalized = np.clip((thermal-min_perc) / (max_perc-min_perc), 0, 1)

    # Colorize thermogram, load as image
    thermal_normalized_color = np.uint8(cm.jet(thermal_normalized)*255)
    img = Image.fromarray(thermal_normalized_color[:,:,:3], "RGB")
    return img


def verify_environment():
    bindir   = "/usr/bin/"
    exiftool = os.path.join(bindir, "exiftool")
    if not os.path.isfile(exiftool):
        raise FileNotFoundError("Exiftool not present in {}!".format(bindir))
    DEBUG = False
    return flir.FlirImageExtractor(exiftool, DEBUG)


if __name__ == "__main__":
    Hittestift().run()
