from typing import List, Tuple
from shapely.geometry import Polygon

class Annotation:
    class_name: str
    polygon: Polygon

    def __init__(self, class_name: str, coords: List[Tuple[int, int]]):
        self.class_name = class_name
        self.polygon = Polygon(coords)

    def dict(self):
        return {
            "class_name": self.class_name,
            "geometry": str(self.polygon)
        }
